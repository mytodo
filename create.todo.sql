USE todo;

CREATE  TABLE category (
  idcategory INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(45) NOT NULL
  );

CREATE  TABLE statuses (
  idstatus INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name ENUM('TODO', 'START', 'DONE', 'CANCEL') NOT NULL
  );

CREATE  TABLE deadline (
  iddeadline INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  date DATE NOT NULL
  );

CREATE  TABLE schedule (
  idschedule INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  date DATE NOT NULL
  );

CREATE  TABLE users (
  iduser INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  fname VARCHAR(45) NOT NULL ,
  lname VARCHAR(45) NOT NULL ,
  job VARCHAR(45) NOT NULL ,
  email VARCHAR(20) NOT NULL ,
  avatar BLOB NULL ,
  nickname VARCHAR(45) NULL ,
  password VARCHAR(65) NOT NULL
  );

CREATE  TABLE boss (
  idboss INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  iduser INT(11)  NOT NULL REFERENCES users (iduser )
  );

CREATE  TABLE priority (
  idpriority INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name ENUM('A', 'B', 'C') NOT NULL
  );

CREATE  TABLE task (
  idtask INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idpriority INT(11)  NULL REFERENCES priority (idpriority ),
  idboss INT(11)  NOT NULL REFERENCES boss (idboss ),
  idschedule INT(11)  NULL REFERENCES schedule (idschedule ),
  iddeadline INT(11)  NULL REFERENCES deadline (iddeadline ),
  idcategory INT(11)  NULL REFERENCES category (idcategory ),
  idstatus INT(11)  NULL REFERENCES statuses (idstatus ),
  name VARCHAR(45) NOT NULL
  );

CREATE  TABLE subtask (
  idsubtask INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idtask INT(11)  NOT NULL REFERENCES task (idtask ),
  idstatus INT(11)  NULL REFERENCES statuses (idstatus ),
  iddeadline INT(11)  NULL REFERENCES deadline (iddeadline ),
  idschedule INT(11)  NULL REFERENCES schedule (idschedule ),
  idpriority INT(11)  NULL REFERENCES priority (idpriority ),
  name VARCHAR(45) NOT NULL
  );

CREATE  TABLE comments (
  idcomment INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idtask INT(11)  NOT NULL REFERENCES task (idtask ),
  idsubtask INT(11)  NOT NULL REFERENCES subtask (idsubtask ),
  comments TEXT NOT NULL
  );

CREATE  TABLE tags (
  idtask INT(11)  NOT NULL,
  tag VARCHAR(45) NOT NULL REFERENCES task (idtask ),
  PRIMARY KEY (idtask, tag)
  );

CREATE  TABLE members (
  idmember INT(11)  NOT NULL AUTO_INCREMENT,
  iduser INT(11)  NOT NULL REFERENCES users (iduser ),
  idtask INT(11)  NOT NULL REFERENCES task (idtask ),
  PRIMARY KEY (idmember, iduser, idtask)
  );
